import readlineSync from 'readline-sync';

let nGuess = 0;
let nComputer = Math.ceil(Math.random()*9); //give a number from 1-9

while(nGuess != nComputer){
    nGuess = readlineSync.question('Guess a number from 1 to 9: ');
}
console.log('Correct! The number was: ' + nGuess);
