const nGrade = 90;

if(nGrade < 55){
    console.log("You failed");
}else if(nGrade < 53){
    console.log("You got a D-");
}else if(nGrade < 57){
    console.log("You got a D");
}else if(nGrade < 60){
    console.log("You got a D+");
}else if(nGrade < 63){
    console.log("You got a C-");
}else if(nGrade < 67){
    console.log("You got a C");
}else if(nGrade < 70){
    console.log("You got a C+");
}else if(nGrade < 73){
    console.log("You got a B-");
}else if(nGrade < 77){
    console.log("You got a B");
}else if(nGrade < 80){
    console.log("You got a B+");
}else if(nGrade < 87){
    console.log("You got an A-");
}else if(nGrade < 95){
    console.log("You got an A");
}
