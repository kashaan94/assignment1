# Welcome, this is Assignment 1

Welcome to your wiki! This is the default page we've installed for your convenience. Go ahead and edit it.

## Private Repository VS. Public Repository

A capstone customer would want to use a private repository due to the fact that they do not want their projects to be seen by the public unless they are dveelopers 
working on the project. When it comes to code it can be confidential and many companies do not want their code let out to the public as it can be coppied and used 
as their own. Open source is only benefitial when you know that many others can use your application and make chnages to it. 

Public repositories are a great choice for getting started! They're visible to any user on your GitHub Enterprise instance, so you can benefit from a collaborative community.
Private repositories require a little more setup. They're only available to you, the repository owner, as well as any collaborators you choose to share with.