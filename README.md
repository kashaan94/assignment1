# Assignment1

Instructions on how to build install and use this project. 

A simple es6 starting point with vscode debugging and synchronous console input.

Steps to install a folder from bitbucket to vscode.

Git clone url name (space) .
Install dependencies by typing npm install 
To run a program press control + fn + f5


Assuming that you have a bitbucket account you will need to git init to initialize a existing Git repository.
You will now need to git clone https://bitbucket.org/kashaan94/assignment1.git .

NOTE: I decided to go with the MIT license is because this is the most popular licence used in open source projects on both
Git and Bitbucket. 
